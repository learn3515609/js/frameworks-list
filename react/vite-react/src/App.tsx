import { useState } from 'react'

type TType = {
  id: number;
  name: string
}

function App() {
  const [name, setName] = useState('')
  const [list, setList] = useState<TType[]>([])

  const handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  }

  const onAdd = () => {
    setList([
      ...list,
      {
        id: +Date.now(),
        name
      }
    ])
    setName('');
  }

  return (
    <>
      <h1>React</h1>
      <ul id="list">
        {list.map(item => <li key={item.id}>{item.name}</li>)}
      </ul>
      <input id="name" value={name} onChange={handleChangeName} /> 
      <button id="btn" onClick={onAdd}>Save</button>
    </>
  )
}

export default App
