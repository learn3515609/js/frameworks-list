import { html, css, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { TType } from './types';



@customElement('todo-list')
export class TodoList extends LitElement {
  static styles = css`p { color: blue }`;

  @property()
  list: TType[] = [];

  public onSubmit(value: string) {
    this.list = [...this.list, {
        id: +new Date(),
        name: value
    }]
  }

  render() {
    return html`
        <h2>Lit</h2>
        <ul>
            ${this.list.map((item) =>
                html`<todo-item name="${item.name}" />`
            )}
        </ul>
        <todo-form .onSubmit=${this.onSubmit.bind(this)} />
    `;
  }
}



@customElement('todo-item')
class TodoItem extends LitElement {

  @property()
  name = '';

  render() {
    return html`
        <li>${this.name}</li>
    `;
  }
}

@customElement('todo-form')
class TodoForm extends LitElement {

    @property()
    onSubmit: Function;

    @property()
    value = '';

    private onButtonClick(e: Event) {
        this.onSubmit(this.value);
        this.value = '';
    }

    private onChangeValue(e: Event) {
        this.value = (e.target as HTMLInputElement).value;
    }

    render() {
        return html`
            <input .value=${this.value} @input=${this.onChangeValue}> <button @click="${this.onButtonClick}">Save</button>
        `;
    }
}