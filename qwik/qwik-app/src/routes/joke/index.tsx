import { component$, useSignal, useTask$  } from '@builder.io/qwik';
import { Form, routeAction$, routeLoader$, server$ } from '@builder.io/qwik-city';
 
export const useDadJoke = routeLoader$(async () => {
  const response = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
    headers: { Accept: 'application/json' },
  });
  return (await response.json()) as {
    id: string;
    completed: boolean;
    title: string;
  };
});

export const useJokeVoteAction = routeAction$((props) => {
    // Предоставляем читателю реализовать это самостоятельно в качестве упражнения.
    console.log('VOTE', props);
  });
 
export default component$(() => {
    const dadJokeSignal = useDadJoke();
    const favoriteJokeAction = useJokeVoteAction();
    const isFavoriteSignal = useSignal(false);

    useTask$(({ track }) => {
        track(() => isFavoriteSignal.value);
        console.log('FAVORITE (isomorphic)', isFavoriteSignal.value);
        server$(() => {
            console.log('FAVORITE (server)', isFavoriteSignal.value);
          })();
      });

      
    return (
      <section class="section bright">
        <p>{dadJokeSignal.value.title}</p>
        <Form action={favoriteJokeAction}>
            <input type="hidden" name="jokeID" value={dadJokeSignal.value.id} />
            <button name="vote" value="up">👍</button>
            <button name="vote" value="down">👎</button>
        </Form>
        <button
            onClick$={() => {
                isFavoriteSignal.value = !isFavoriteSignal.value;
            }}>
            {isFavoriteSignal.value ? '❤️' : '🤍'}
        </button>
      </section>
    );
  });