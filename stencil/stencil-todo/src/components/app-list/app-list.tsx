import { Component, State, h, Listen, Event, EventEmitter } from '@stencil/core';
import { Item } from '../app-item/app-item';

// @Component({
//   tag: 'app-home',
//   styleUrl: 'app-home.css',
//   shadow: true,
// })
// export class AppHome {
//   render() {
//     return (
//       <div class="app-home">
//         <p>
//           Welcome to the Stencil App Starter. You can use this starter to build entire apps all with web components using Stencil! Check out our docs on{' '}
//           <a href="https://stenciljs.com">stenciljs.com</a> to get started.
//         </p>

//         <stencil-route-link url="/profile/stencil">
//           <button>Profile page</button>
//         </stencil-route-link>
//       </div>
//     );
//   }
// }


@Component({
  tag: 'app-list',
  styleUrl: 'app-list.css',
  shadow: true
})
export class AppList {

  @State() items: Item[] = [{
    id: 1,
    title: 'first'
  }];

  @Listen('todoAdded')
  todoAddedHandler(item: CustomEvent<Item>) {
    if(!item.detail.title.trim()) return;
    this.items = [...this.items, item.detail];
  }

  render() {
    return (
      <div>
        <ul>
          {this.items.map(item => <app-item item={item} />)}
        </ul>
        <app-form  />
      </div>
    )
  }
}