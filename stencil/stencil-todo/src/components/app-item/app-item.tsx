import { Component, h, Prop } from '@stencil/core';

export type Item = {
    id: number;
    title: string;
}

@Component({
    tag: 'app-item',
    shadow: true
  })
  export class AppList {

    @Prop() item: Item;

  
    render() {
      return (
        <li>{this.item.title}</li>
      )
    }
  }