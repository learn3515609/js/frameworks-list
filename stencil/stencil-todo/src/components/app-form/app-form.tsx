import { Component, h, Event, EventEmitter, State } from '@stencil/core';

export type Item = {
    id: number;
    title: string;
}

@Component({
    tag: 'app-form',
    styleUrl: 'app-form.css',
    shadow: true
  })
  export class AppList {

    @Event() todoAdded: EventEmitter<Item>;

    @State() value: string = '';

    todoAddedHandler() {
        this.todoAdded.emit({
            id: +Date.now(),
            title: this.value
        });
        this.value = '';
    }

    handleChange(event) {
        this.value = event.target.value;
    }



  
    render() {
      return (
        <div>
            <input value={this.value} type="text" onInput={(event) => this.handleChange(event)} />
            <button onClick={this.todoAddedHandler.bind(this)}>Add</button>
        </div>
      )
    }
  }