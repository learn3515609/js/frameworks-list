import { fromEvent } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

const btn = document.getElementById('btn');
const name = document.getElementById('name');
const list = document.getElementById('list');

const inputStream$ = fromEvent(name, 'input').pipe(
    //debounceTime(500),
    map(event => (event.target as HTMLInputElement).value),
    switchMap((v) => 
        fromEvent(btn, 'click').pipe(
            map(() => {
                const li = document.createElement('li');
                li.textContent = v;
                return li;
            })
        )
    )
)

inputStream$.subscribe(item => list.append(item))